CREATE TYPE user_type AS ENUM ('user', 'store', 'admin');

CREATE TABLE IF NOT EXISTS users(
  user_id varchar(36) PRIMARY KEY,
  token VARCHAR (255) NOT NULL,
  user_type user_type NOT NULL
);
