package repository

import (
	"database/sql"

	dm "gitlab.com/mangbinbin/services/notification-service/service/domain"
)

// IUserRepository interface
type IUserRepository interface {
	Store(user dm.User) error
	GetByID(id string) (dm.User, error)
}

// UserRepository struct
type UserRepository struct {
	db *sql.DB
}

// NewUserRepository method
func NewUserRepository(db *sql.DB) IUserRepository {
	return &UserRepository{
		db: db,
	}
}

// Store method
func (r *UserRepository) Store(user dm.User) error {
	query := `
		INSERT INTO users (user_id, token, user_type) VALUES ($1, $2, $3)
		ON CONFLICT (user_id) DO UPDATE SET token = $2
	`

	_, err := r.db.Query(query, user.ID, user.Token, user.Type)

	return err
}

// GetByID method
func (r *UserRepository) GetByID(id string) (dm.User, error) {
	query := `SELECT user_id, token, user_type FROM users WHERE ID = $1`

	row := r.db.QueryRow(query, id)

	user := dm.User{}

	err := row.Scan(
		&user.ID,
		&user.Token,
		&user.Type,
	)

	if err != nil {
		return dm.User{}, nil
	}

	return user, nil
}
