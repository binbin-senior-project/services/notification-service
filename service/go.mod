module gitlab.com/mangbinbin/services/notification-service/service

go 1.13

require (
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/golang/protobuf v1.4.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.4.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	google.golang.org/grpc v1.29.1
)
