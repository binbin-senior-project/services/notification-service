package database

import (
	"database/sql"
	"fmt"
	"os"
)

// NewConnectDatabase is a initial method
// called by Client (main.go)
func NewConnectDatabase() *sql.DB {

	var (
		user     = os.Getenv("POSTGRES_USER")
		password = os.Getenv("POSTGRES_PASSWORD")
		host     = os.Getenv("POSTGRES_HOST")
		port     = os.Getenv("POSTGRES_PORT")
		database = os.Getenv("POSTGRES_DB")
	)

	psqlInfo := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, database)

	db, err := sql.Open("postgres", psqlInfo)

	if err != nil {
		panic(err)
	}

	err = db.Ping()

	if err != nil {
		panic(err)
	}

	// return *pg.DB to called in repository
	return db
}
