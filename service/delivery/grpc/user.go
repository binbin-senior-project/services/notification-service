package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/notification-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/notification-service/service/domain"
)

// SetToken method
func (d *NotificationDelivery) SetToken(ctx context.Context, req *pb.SetTokenRequest) (*pb.SetTokenReply, error) {
	user := dm.User{
		ID:    req.UserID,
		Token: req.Token,
		Type:  req.Type,
	}

	err := d.usecase.User.SetToken(user)

	if err != nil {
		return &pb.SetTokenReply{Success: false}, err
	}

	return &pb.SetTokenReply{Success: true}, nil
}
