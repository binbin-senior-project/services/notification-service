package amqp

import (
	amqp "github.com/streadway/amqp"
	dm "gitlab.com/mangbinbin/services/notification-service/service/domain"
	h "gitlab.com/mangbinbin/services/notification-service/service/helper"
	u "gitlab.com/mangbinbin/services/notification-service/service/usecase"
)

// AMQPSubscribe struct
type AMQPSubscribe struct {
	usecase u.RegisterUsecase
	channel *amqp.Channel
}

// NewAMQPSubscribe method
func NewAMQPSubscribe(usecase u.RegisterUsecase, channel *amqp.Channel) {
	subscriber := AMQPSubscribe{
		usecase: usecase,
		channel: channel,
	}

	go subscriber.SubscribeMessagingEvent()
}

// SubscribeMessagingEvent method
func (d *AMQPSubscribe) SubscribeMessagingEvent() {
	msgs, err := d.channel.Consume(
		"messaging@notification.queue", // queue
		"",                             // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)

	failOnError(err, "Failed to register a SubscribeNewMessagingEvent consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			n := dm.NotificationCreatedEvent{}
			err = h.DeserializePayload(&n, msg.Body)

			notification := dm.Notification{
				UserID: n.UserID,
				Title:  n.Title,
				Body:   n.Body,
			}

			d.usecase.Notification.PushNotification(notification, n.UserType)
		}
	}()

	<-forever
}
