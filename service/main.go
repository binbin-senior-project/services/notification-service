package main

import (
	"github.com/joho/godotenv"

	db "gitlab.com/mangbinbin/services/notification-service/service/database"
	amqp "gitlab.com/mangbinbin/services/notification-service/service/delivery/amqp"
	grpc "gitlab.com/mangbinbin/services/notification-service/service/delivery/grpc"
	r "gitlab.com/mangbinbin/services/notification-service/service/repository"
	u "gitlab.com/mangbinbin/services/notification-service/service/usecase"

	_ "github.com/lib/pq"
)

func main() {
	godotenv.Load()
	database := db.NewConnectDatabase()

	repo := r.RegisterRepository{
		User: r.NewUserRepository(database),
	}

	usecase := u.RegisterUsecase{
		User:         u.NewUserUsecase(repo),
		Notification: u.NewNotificationUsecase(repo),
	}

	// Register Broker Client
	ch := amqp.NewNotificationDelivery()

	// Register Subscriber broker
	amqp.NewAMQPSubscribe(usecase, ch)

	// Register Publisher broker
	broker := amqp.NewAMQPPublish(ch)

	// Register GRPC Server
	grpc.NewNotificationDelivery(broker, usecase)
}
