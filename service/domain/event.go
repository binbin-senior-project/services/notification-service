package domain

// NotificationCreatedEvent struct
type NotificationCreatedEvent struct {
	UserID string
	Title  string
	Body   string
}
