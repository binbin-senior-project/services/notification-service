package domain

// User struct
type User struct {
	ID    string
	Token string
	Type  string
}

// Notification struct
type Notification struct {
	ID        string
	Title     string
	Body      string
	CreatedAt string
	UserID    string
}
