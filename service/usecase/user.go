package usecase

import (
	dm "gitlab.com/mangbinbin/services/notification-service/service/domain"
	r "gitlab.com/mangbinbin/services/notification-service/service/repository"
)

// IUserUsecase interface
type IUserUsecase interface {
	SetToken(user dm.User) error
}

// UserUsecase is a struct
type UserUsecase struct {
	repo r.RegisterRepository
}

// NewUserUsecase method
func NewUserUsecase(repo r.RegisterRepository) IUserUsecase {
	return &UserUsecase{
		repo: repo,
	}
}

// SetToken method
func (u *UserUsecase) SetToken(user dm.User) error {
	err := u.repo.User.Store(user)

	return err
}
