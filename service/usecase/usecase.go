package usecase

// RegisterUsecase struct
type RegisterUsecase struct {
	Notification INotificationUsecase
	User         IUserUsecase
}
