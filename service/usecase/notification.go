package usecase

import (
	"errors"
	"os"
	"strings"

	"github.com/NaySoftware/go-fcm"
	dm "gitlab.com/mangbinbin/services/notification-service/service/domain"
	r "gitlab.com/mangbinbin/services/notification-service/service/repository"
)

// INotificationUsecase interface
type INotificationUsecase interface {
	PushNotification(notification dm.Notification, role string) error
}

// NotificationUsecase is a struct
type NotificationUsecase struct {
	repo r.RegisterRepository
}

// NewNotificationUsecase method
func NewNotificationUsecase(repo r.RegisterRepository) INotificationUsecase {
	return &NotificationUsecase{
		repo: repo,
	}
}

// PushNotification method
func (u *NotificationUsecase) PushNotification(notification dm.Notification, role string) error {
	user, err := u.repo.User.GetByID(notification.UserID)

	if err != nil {
		return err
	}

	p := &fcm.NotificationPayload{
		Title: strings.ToUpper(notification.Title),
		Body:  notification.Body,
		Sound: "default",
	}

	ids := []string{user.Token}
	var key string

	if user.Type == "user" {
		key = os.Getenv("USER_MESSAGING_KEY")
	} else if user.Type == "store" {
		key = os.Getenv("STORE_MESSAGING_KEY")
	} else {
		return errors.New("No User Type")
	}

	c := fcm.NewFcmClient(key)
	c.SetNotificationPayload(p)
	c.AppendDevices(ids)

	_, err = c.Send()

	if err != nil {
		return err
	}

	return nil
}
